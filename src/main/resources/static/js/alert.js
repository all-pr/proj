const alertPlaceholder = document.getElementById('liveAlertPlaceholder')
const appendAlert = (message, type) => {
  const wrapper = document.createElement('div')
  wrapper.innerHTML = [
    `<div class="alert alert-${type} alert-dismissible" role="alert">`,
    `   <div>${message}</div>`,
    '   <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>',
    '</div>'
  ].join('')

  alertPlaceholder.append(wrapper)
}

if (alertPlaceholder != null) {
    var alertText = alertPlaceholder.getAttribute('name')
    appendAlert(alertText, 'danger')
}


//---------------

const alertSpinner = document.getElementById('runTestDiv')
const spin = () => {
  const wrapper = document.createElement('div')
  wrapper.innerHTML = [
    `<div class="text-center">`,
    `<div class="spinner-border" role="status">`,
        `   <span class="visually-hidden">Loading...</span>`,
        '</div>',
    '</div>',
  ].join('')

  alertSpinner.append(wrapper)
}

const alertTrigger = document.getElementById('runBtn')
if (alertTrigger) {
  alertTrigger.addEventListener('click', () => {
      spin()
    })
}

const updateTrigger = document.getElementById('updateBtn')
if (updateTrigger) {
  updateTrigger.addEventListener('click', () => {
      spin()
    })
}
