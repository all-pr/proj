//const alertMail = document.getElementById('sendMailDiv')
//const alertMailMessage = () => {
//  const wrapper = document.createElement('div')
//  wrapper.innerHTML = [
//      `<div class="alert alert-danger alert-dismissible" role="alert">`,
//      `   <div>Sending...</div>`,
//      '   <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>',
//      '</div>'
//    ].join('')
//
//  alertMail.append(wrapper)
//}


(() => {
  'use strict'

  // Fetch all the forms we want to apply custom Bootstrap validation styles to
  const forms = document.querySelectorAll('.needs-validation')

  // Loop over them and prevent submission
  Array.from(forms).forEach(form => {
    form.addEventListener('submit', event => {
      if (!form.checkValidity()) {
        event.preventDefault()
        event.stopPropagation()
      }

      form.classList.add('was-validated')

    }, false)
  })

})()

//-------------------

function submitForm(event) {

    var formData = $("#emailform").serialize();

    var buttonId = event.submitter.id; // Получите id кнопки, которая была нажата

    var url; // Здесь хранится URL для отправки запроса

    if (buttonId === "previewBtn") {
        event.preventDefault();
        url = "/email/preview";
        $.ajax({
            type: "GET",
            url: url,
            data: formData,
            success: function(response) {
                $("#responseMessage").html(response);
            },
            error: function() {
                  $("#responseMessage").text("Произошла ошибка при отправке данных.");
            }
        });

    } else if (buttonId === "sendBtn") {
        url = "/email/sender";
        $.ajax({
                    type: "POST",
                    url: url,
                    data: formData,
                    success: function(response) {

                    }
                });
    }

}

