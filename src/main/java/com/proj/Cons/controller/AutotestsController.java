package com.proj.Cons.controller;


import com.proj.Cons.dto.TestCase;
import com.proj.Cons.dto.TestCaseList;
import com.proj.Cons.rep.TestCaseListRep;
import com.proj.Cons.rep.TestCaseRep;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;

@Controller
public class AutotestsController {

    private static Logger logger = LoggerFactory.getLogger(AutotestsController.class);

    @Autowired
    TestCaseListRep testCaseListRep;

    @Autowired
    TestCaseRep testCaseRep;

    @GetMapping("/acq/autotests/list")
    public String autotestList(Model model) {
        Iterable<TestCaseList> testCaseLists = testCaseListRep.findAll();
        model.addAttribute("autotest", testCaseLists);
        return "acq-autotests-list";
    }

    @GetMapping("/acq/autotests/add")
    public String autotestAdd(Model model) {
        return "acq-autotests-add";
    }

    @PostMapping("/acq/autotests/add")
    public String autotestPost(@RequestParam String testCaseName, Model model, HttpSession session) {

        session.setAttribute("sessionName", testCaseName);
        session.setAttribute("url", "/acq/autotests/add");

        TestCaseList testCaseList = new TestCaseList(testCaseName);
        testCaseListRep.save(testCaseList);

        logger.info("autotest {} has been added", testCaseList.getTestCaseName());

        Long id = testCaseList.getId();

        return "redirect:/acq/testcase/"+ id +"/list";
    }

    @PostMapping("/acq/autotests/{id}/remove")
    public String autotestRemove(@PathVariable(value = "id") long id, Model model){

        TestCaseList testCaseList = testCaseListRep.findById(id).orElseThrow();
        if (!testCaseList.getTestCases().isEmpty()){

            Iterable<TestCase> testCases = testCaseRep.findAll();
            testCases.forEach(tc -> testCaseRep.deleteAll());

        }

        testCaseListRep.delete(testCaseList);

        logger.info("autotest {} has been deleted", testCaseList.getTestCaseName());

        return "redirect:/acq/autotests/list";
    }

    @PostMapping("/acq/autotests/del-all")
    public String autotestRemoveAll(){

        Iterable<TestCaseList> testCaseList = testCaseListRep.findAll();
        testCaseList.forEach(tc -> testCaseListRep.deleteAll());

        logger.info("all testcases have been deleted");

        return "redirect:/acq/autotests/list";
    }


}
