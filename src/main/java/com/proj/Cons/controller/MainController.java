package com.proj.Cons.controller;

import com.proj.Cons.annotations.CustomExceptionHandlerForEpay;
import com.proj.Cons.epayCons.EpayCons;
import com.proj.Cons.nwintCons.NwintCons;
import com.proj.Cons.serverProcesses.ServerProcesses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

@Controller
@CustomExceptionHandlerForEpay
public class MainController {

    private static Logger logger = LoggerFactory.getLogger(MainController.class);

    @Autowired
    EpayCons epayCons;

    @Autowired
    NwintCons nwintCons;

    @Autowired
    ServerProcesses serverProcesses;

    @Value("${sv.fields}")
    private String svPathToFields;

    @Value("${mastercard.fields}")
    private String mastercardPathToFields;

    @Value("${sender.status}")
    private String senderStatus;

    @GetMapping("/")
    public String home(Model model){
        model.addAttribute("title", "Epay Constructor");
        model.addAttribute("senderStatus", senderStatus);
        return "home";
    }

    @RequestMapping(value = "/getepay", method = {RequestMethod.GET, RequestMethod.POST})
    public String epayConstructor(@RequestBody String body,
                                  HttpSession session,
                                  Model model) throws Exception {
        logger.info("There is starting /getepay");
        session.setAttribute("url", "getter");
        String isoMessage = epayCons.buildISOMessage(body, svPathToFields);

        model.addAttribute("body", isoMessage);

        logger.info("isoMessage -> {} \n", isoMessage);

        serverProcesses.putIntoData(isoMessage);
        serverProcesses.startCrTxn();
        logger.info("/getepay has been finished");

        return "getepay";
    }

    @RequestMapping(value = "/getjson", method = {RequestMethod.GET, RequestMethod.POST})
    public String jsonConstructor(@RequestBody String body, HttpSession session, Model model) throws Exception {
        logger.info("There is starting /getjson");
        session.setAttribute("url", "getter");

        String jsonMessage = epayCons.unpackedISOMessage(body, svPathToFields);

        model.addAttribute("body", jsonMessage);

        logger.info("/getjson has been finished");

        return "getepay";
    }

    @RequestMapping(value = "/getjsonfrommas", method = {RequestMethod.GET, RequestMethod.POST})
    public String jsonFromMas(@RequestBody String body, HttpSession session, Model model) throws Exception {
        logger.info("There is starting /jsonFromMas");
        session.setAttribute("url", "getter");

        String jsonMessage = nwintCons.unpackedMastercardMessage(body);

        model.addAttribute("body", jsonMessage);

        logger.info("/jsonFromMas has been finished");

        return "getepay";
    }



}
