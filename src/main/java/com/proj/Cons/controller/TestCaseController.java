package com.proj.Cons.controller;

import com.proj.Cons.annotations.CustomExceptionForTestCases;
import com.proj.Cons.dto.TestCase;
import com.proj.Cons.dto.TestCaseList;
import com.proj.Cons.rep.TestCaseListRep;
import com.proj.Cons.rep.TestCaseRep;
import com.proj.Cons.service.TestCaseControllerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
@CustomExceptionForTestCases
public class TestCaseController {

    private static Logger logger = LoggerFactory.getLogger(TestCaseController.class);

    @Autowired
    TestCaseControllerService testCaseControllerService;

    @Autowired
    TestCaseListRep testCaseListRep;

    @Autowired
    TestCaseRep testCaseRep;


    @GetMapping("/acq/testcase/{id}/list")
    public String autotestDetails(@PathVariable(value = "id") long id, Model model) {
        if (!testCaseListRep.existsById(id)) {
            return "redirect:/acq/autotests/list";
        }

        TestCaseList testCaseList = testCaseListRep.findById(id).orElseThrow();

        List<TestCase> testCase = testCaseList.getTestCases();

        model.addAttribute("testCase", testCase);
        model.addAttribute("testCaseName", testCaseList.getTestCaseName());
        model.addAttribute("autotestID", id);
        return "acq-testcase-list";
    }

    @GetMapping("/acq/testcase/{autotestID}/add")
    public String epayAdd(@PathVariable(value = "autotestID") long autotestID, Model model) {
        model.addAttribute("autotestID", autotestID);

        return "acq-testcase-add";
    }

    @PostMapping("/acq/testcase/{autotestID}/add")
    public String epayPost(@PathVariable(value = "autotestID") long autotestID,
                           @RequestParam String name,
                           @RequestParam String jsonBody,
                           Model model,
                           HttpSession session) throws Exception {

        session.setAttribute("sessionTransactionName", name);
        session.setAttribute("sessionJsonBody", jsonBody);
        session.setAttribute("url", "acq-testcase-add");
        session.setAttribute("sessionAutotestID", autotestID);

        testCaseRep.save(testCaseControllerService.addTransaction(autotestID, name, jsonBody));

        return "redirect:/acq/testcase/{autotestID}/list";
    }

    @GetMapping("/acq/testcase/{autotestID}/epay/{id}/details")
    public String testCaseDetails(@PathVariable(value = "id") long id,
                                  @PathVariable(value = "autotestID") long autotestID, Model model) {
        if (!testCaseRep.existsById(id)) {
            return "redirect:/acq/testcase/{autotestID}/list";
        }

        ArrayList<TestCase> testCaseDetails = testCaseControllerService.getTestCaseDetails(id);
        model.addAttribute("epay", testCaseDetails);
        model.addAttribute("autotestID", autotestID);

        return "acq-testcase-details";

    }

    @GetMapping("/acq/testcase/{autotestID}/epay/{id}/edit")
    public String epayEdit(@PathVariable(value = "id") long id,
                           @PathVariable(value = "autotestID") long autotestID, Model model) {

        if (!testCaseRep.existsById(id)) {
            return "redirect:/acq/testcase/{autotestID}/list";
        }

        ArrayList<TestCase> testCaseDetails = testCaseControllerService.getTestCaseDetails(id);
        model.addAttribute("epay", testCaseDetails);
        model.addAttribute("autotestID", autotestID);

        return "acq-testcase-edit";
    }

    @PostMapping("/acq/testcase/{autotestID}/epay/{id}/edit")
    public String epayUpdate(@PathVariable(value = "id") long id,
                             @PathVariable(value = "autotestID") long autotestID,
                             @RequestParam String name, @RequestParam String jsonBody,
                             Model model, HttpSession session) throws Exception {

        TestCase testCase = testCaseRep.findById(id).orElseThrow();
        if (!jsonBody.equals(testCase.getJsonBody())) {
            session.setAttribute("sessionEpayDetails", testCase);
            session.setAttribute("url", "acq-testcase-edit");
            session.setAttribute("sessionAutotestID", autotestID);

            testCaseRep.save(testCaseControllerService.testCaseEdit(testCase, name, jsonBody));

            logger.info("transaction {} has been edited", name);

            return "redirect:/acq/testcase/{autotestID}/epay/{id}/details";
        }

        logger.info("transaction {} has been edited", name);
        testCase.setTransactionName(name);
        testCaseRep.save(testCase);

        return "redirect:/acq/testcase/{autotestID}/epay/{id}/details";
    }

    @PostMapping("/acq/testcase/{autotestID}/epay/{id}/remove")
    public String epayRemove(@PathVariable(value = "id") long id,
                             @PathVariable(value = "autotestID") long autotestID,
                             Model model, HttpSession session) {

        session.setAttribute("url", "acq-testcase-list");
        session.setAttribute("sessionAutotestID", autotestID);

        TestCase testCase = testCaseRep.findById(id).orElseThrow();
        testCaseRep.delete(testCase);
        logger.info("transaction {} has been deleted", testCase.getTransactionName());
        return "redirect:/acq/testcase/{autotestID}/list";
    }

    @PostMapping("/acq/testcase/{autotestID}/epay/{id}/resend")
    public String epayResend(@PathVariable(value = "id") long id,
                             @PathVariable(value = "autotestID") long autotestID,
                             Model model, HttpSession session) throws Exception {

        TestCase testCase = testCaseRep.findById(id).orElseThrow();

        session.setAttribute("url", "acq-testcase-details");
        session.setAttribute("sessionAutotestID", autotestID);
        session.setAttribute("sessionEpayDetails", testCase);

        testCaseControllerService.sendToSV(testCase);

        logger.info("transaction {} has been resended", testCase.getTransactionName());

        return "redirect:/acq/testcase/{autotestID}/epay/{id}/details";
    }

    @PostMapping("/acq/testcase/{autotestID}/epay/{id}/generateNwint")
    public String generateNwint(@PathVariable(value = "id") long id,
                                @PathVariable(value = "autotestID") long autotestID,
                                Model model, HttpSession session) throws Exception {

        TestCase testCase = testCaseRep.findById(id).orElseThrow();

        session.setAttribute("url", "acq-testcase-updateStan");
        session.setAttribute("sessionEpayDetails", testCase);
        session.setAttribute("sessionAutotestID", autotestID);

        testCaseControllerService.getNwintFields(testCase);

        return "redirect:/acq/testcase/{autotestID}/epay/{id}/details";
    }


    @PostMapping("/acq/testcase/{autotestID}/run")
    public String testcaseRun(@PathVariable(value = "autotestID") long autotestID,
                              Model model, HttpSession session) throws Exception {

        TestCaseList testCaseList = testCaseListRep.findById(autotestID).orElseThrow();
        List<TestCase> testCase = testCaseList.getTestCases();

        session.setAttribute("url", "acq-testcase-list");
        session.setAttribute("sessionAutotestID", autotestID);
        session.setAttribute("sessionTestCase", testCase);

        for (TestCase eachTestCase : testCase) {

            testCaseControllerService.sendToSV(eachTestCase);

            eachTestCase.setNwintJsonBody(null);
            eachTestCase.setNwintHexBody(null);

            logger.info("name {}\n hex body {} \n", eachTestCase.getTransactionName(), eachTestCase.getHexBody());
        }


        return "redirect:/acq/testcase/{autotestID}/list";
    }

    @PostMapping("/acq/testcase/{autotestID}/getallnwint")
    public String getAllNwint(@PathVariable(value = "autotestID") long autotestID,
                              Model model, HttpSession session) throws Exception {

        TestCaseList testCaseList = testCaseListRep.findById(autotestID).orElseThrow();
        List<TestCase> testCase = testCaseList.getTestCases();

        session.setAttribute("url", "acq-testcase-list");
        session.setAttribute("sessionAutotestID", autotestID);
        session.setAttribute("sessionTestCase", testCase);

        for (TestCase eachTestCase : testCase) {

            testCaseControllerService.getNwintFields(eachTestCase);

            logger.info("name {}", eachTestCase.getTransactionName());
        }


        return "redirect:/acq/testcase/{autotestID}/list";
    }

    @GetMapping("/acq/testcase/{autotestID}/import")
    public String epayImport(@PathVariable(value = "autotestID") long autotestID, Model model) {
        model.addAttribute("autotestID", autotestID);

        return "acq-testcase-import";
    }

    @PostMapping("/acq/testcase/{autotestID}/import")
    public String epayImportPost(@PathVariable(value = "autotestID") long autotestID,
                                 @RequestParam String name,
                                 @RequestParam String hexBody,
                                 Model model, HttpSession session) throws Exception {

        session.setAttribute("sessionTransactionName", name);
        session.setAttribute("sessionHexBody", hexBody);
        session.setAttribute("url", "acq-testcase-import");
        session.setAttribute("sessionAutotestID", autotestID);

        TestCase testCase = testCaseControllerService.importTransaction(autotestID, name, hexBody);
        testCaseRep.save(testCase);
        Long id = testCase.getId();

        return "redirect:/acq/testcase/{autotestID}/epay/" + id + "/details";
    }

    @PostMapping("/acq/testcase/{autotestID}/epay/{id}/generateStan")
    public String generateStan(@PathVariable(value = "id") long id,
                               @PathVariable(value = "autotestID") long autotestID,
                               Model model, HttpSession session) throws Exception {

        TestCase testCase = testCaseRep.findById(id).orElseThrow();

        session.setAttribute("url", "acq-testcase-updateStan");
        session.setAttribute("sessionEpayDetails", testCase);
        session.setAttribute("sessionAutotestID", autotestID);

        testCaseControllerService.generateNewStan(testCase);

        return "redirect:/acq/testcase/{autotestID}/epay/{id}/details";
    }

    @PostMapping("/acq/testcase/{autotestID}/updateAllStan")
    public String updateAllStan(@PathVariable(value = "autotestID") long autotestID,
                                Model model, HttpSession session) throws Exception {

        TestCaseList testCaseList = testCaseListRep.findById(autotestID).orElseThrow();
        List<TestCase> testCase = testCaseList.getTestCases();

        session.setAttribute("url", "acq-testcase-list");
        session.setAttribute("sessionAutotestID", autotestID);
        session.setAttribute("sessionTestCase", testCase);

        for (TestCase eachTestCase : testCase) {

            testCaseControllerService.generateNewStan(eachTestCase);

        }

        return "redirect:/acq/testcase/{autotestID}/list";
    }

}
