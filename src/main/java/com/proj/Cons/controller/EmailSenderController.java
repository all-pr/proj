package com.proj.Cons.controller;

import com.proj.Cons.annotations.CustomExceptionForEmailSender;
import com.proj.Cons.model.MailFields;
import com.proj.Cons.service.OracleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

@Controller
@CustomExceptionForEmailSender
public class EmailSenderController {

    private static Logger logger = LoggerFactory.getLogger(EmailSenderController.class);

    @Autowired
    OracleService oracleService;

    @PostMapping(value = "/sendemail")
    public ResponseEntity<MailFields> sender(@RequestBody MailFields mailFields) throws Exception {
        logger.info("There is starting SENDER");

        oracleService.executeProcedure(mailFields);

        logger.info("SENDER has been finished");

        return ResponseEntity.ok(mailFields);
    }

    @GetMapping(value = "/checkemail", produces = MediaType.TEXT_HTML_VALUE)
    @ResponseBody
    public String checker(@RequestBody MailFields mailFields) {
        logger.info("There is starting CHECKER");

        logger.info("SENDER has been CHECKER");

        return oracleService.executeCheckProcedure(mailFields);
    }

    @GetMapping("/email/builder")
    public String emailBuild(Model model) {
       model.addAttribute("mailFields", new MailFields());
        return "email-builder";
    }

    @GetMapping("/email/sender")
    public String emailSend(@ModelAttribute("mailFields") MailFields mailFields, HttpSession session) throws Exception {

        logger.info("There is starting SENDER");

        session.setAttribute("url", "email-builder");
        session.setAttribute("sessionMailFields", mailFields);

        oracleService.executeProcedure(mailFields);

        logger.info("SENDER has been finished");

        return "redirect:/email/builder";
    }

    @GetMapping("/email/preview")
    public String test(@ModelAttribute("mailFields") MailFields mailFields, Model model) {

        logger.info("There is starting PREVIEW");

        String message = "Please fill in the fields";

        if (mailFields.getIssuerBin().isEmpty()||mailFields.getRespcode().isEmpty()||
                mailFields.getSenderDetails().isEmpty()||mailFields.getIssuerDetails().isEmpty()||
                mailFields.getFooter().isEmpty()){
            model.addAttribute("message", message);
        } else {
            message = oracleService.executeCheckProcedure(mailFields);
            model.addAttribute("message", message);
        }

        logger.info("PREVIEW has been finished");

        return "email-preview";
    }


}
