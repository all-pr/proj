package com.proj.Cons.nwintCons;

import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;

@Component
public class DecodeMastercardMes {

    private static Logger logger = LoggerFactory.getLogger(DecodeMastercardMes.class);

    public String toEbcdic(String strToConvert) throws IOException {
        String newHex = strToConvert.replaceAll("\\.", "");
        newHex = newHex.replaceAll("\\s", "");
        logger.info("newHex -> {}", newHex);

        String bitMap = newHex.substring(8, newHex.indexOf("F1F6"));
        logger.info("bitMap {}", bitMap);

        String ebcdicBitMap = getEbcdicBitMap(bitMap);

        newHex = newHex.replace(bitMap, ebcdicBitMap);
        logger.info("final hex {}", newHex);

        logger.info("hex -> {}", newHex);

        String[] test = newHex.split( "(?<=\\G..)" );
        ByteBuffer sb = ByteBuffer.allocate( test.length );
        for ( String s : test )
            sb.put( (byte) Short.parseShort( s, 16 ) );
        return new String( sb.array(), "CP1047");
    }

    private String getEbcdicBitMap(String bitMap) throws UnsupportedEncodingException {
        String ebcdicBitMap;
        byte[] inputBytes = bitMap.getBytes("CP1047");
        byte[] ebcdicBytes = new byte[inputBytes.length];

        for (int i = 0; i < inputBytes.length; i++) {
            ebcdicBytes[i] = inputBytes[i];
        }

        ebcdicBitMap = Hex.encodeHexString(ebcdicBytes);

        logger.info("ebcdicMap -> {}", ebcdicBitMap );
        return ebcdicBitMap;
    }

}
