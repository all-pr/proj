package com.proj.Cons.nwintCons;

import com.proj.Cons.service.NwintLogSearchService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Component
public class NwintStream {

    private static Logger logger = LoggerFactory.getLogger(NwintStream.class);

    @Autowired
    NwintLogSearchService nwintLogSearchService;

    @Value("${path.to.nwint}")
    private String path;

    @Value("${contour}")
    private String contour;

    @Value(("${nwint1}"))
    private String nwint1;
    @Value(("${nwint2}"))
    private String nwint2;
    @Value(("${nwint3}"))
    private String nwint3;
    @Value(("${nwint4}"))
    private String nwint4;


    CompletableFuture<Object> combinedResult;

    List<String> nwintFiles;
    List<CompletableFuture<?>> streams;

    public String search (String utrnno) throws ExecutionException, InterruptedException {

        if (utrnno == null){
            logger.info("utrnno is 'null' ");
            return null;

        }

        if (contour.equals("test")){
            nwintFiles = List.of(nwint1);
        } else if (contour.equals("stage")){
            nwintFiles = List.of(nwint1, nwint2, nwint3, nwint4);
        } else {
            logger.error("value \"contur\" is incorrect");
        }

        streams = new ArrayList<>();

        for (String nwintFile : nwintFiles) {
            CompletableFuture<String> file1Result = nwintLogSearchService.Pars(path + nwintFile, utrnno);
            streams.add(file1Result);

//            combinedResult = CompletableFuture.anyOf(file1Result);
//
//            if (combinedResult.get() != null) {
//                logger.info("nwint name -> {}", nwintFile);
//                break;
//            }
        }

        CompletableFuture<Object> res = CompletableFuture.anyOf(streams.stream()
                .map(rf -> rf
                        .exceptionally(th -> null)
                        .thenCompose(rr -> rr == null ? null : CompletableFuture.completedFuture(rr)))
                .toArray(CompletableFuture[]::new));

        try {
            String hex = (String) res.get();
            if (hex != null) {
                logger.info("hex was found in nWint");
                return hex;
            } else {
                logger.info("there is no hex in all nWint files");
                return null;
            }
        } catch (Exception e) {
            logger.warn("error during nWint thread operation", e);
            return null;
        }
    }
}
