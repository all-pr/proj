package com.proj.Cons.nwintCons;

import com.proj.Cons.parser.JsonMessagePrint;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.packager.GenericPackager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;

@Component
public class NwintCons {

    @Autowired
    DecodeMastercardMes decodeMastercardMes;

    @Value("${mastercard.fields}")
    private String path;

    JsonMessagePrint jsonMessagePrint = new JsonMessagePrint();

    private static Logger logger = LoggerFactory.getLogger(NwintCons.class);

    public String unpackedMastercardMessage(String stan) throws Exception {

        //String svHex = nwintParser.Pars(stan);
        String mes = decodeMastercardMes.toEbcdic(stan);
        byte [] byteMes = mes.getBytes(StandardCharsets.UTF_8);

        try {
            GenericPackager packager = new GenericPackager(path);
            ISOMsg isoMsg = new ISOMsg();
            isoMsg.setPackager(packager);
            isoMsg.unpack(byteMes);
            String jsonMessage = jsonMessagePrint.printJsonMessage(isoMsg);
            //printISOMessage(isoMsg);

            //byte[] result = isoMsg.pack();
            return jsonMessage;
        } catch(Exception e) {
            throw new Exception(e);
        }

    }

    private void printISOMessage(ISOMsg isoMsg) {
        try {
            logger.info("MTI = {}", isoMsg.getMTI());
            for (int i = 1; i <= isoMsg.getMaxField(); i++) {
                if (isoMsg.hasField(i)) {
                    logger.info("Field {} = {}", i, isoMsg.getString(i));
                }
            }
        } catch (ISOException e) {
            e.printStackTrace();
        }
    }
}
