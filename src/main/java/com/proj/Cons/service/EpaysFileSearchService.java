package com.proj.Cons.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FilenameFilter;
import java.sql.Time;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service
public class EpaysFileSearchService {

    private static Logger logger = LoggerFactory.getLogger(EpaysFileSearchService.class);

    public List<String> findTodaysFiles(String directoryPath) {
        List<String> todaysFiles = new ArrayList<>();
        LocalDateTime today = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
        Time nowTime = Time.valueOf(formatter.format(today));
        logger.info("today -> {}", nowTime);


        File directory = new File(directoryPath);

        FilenameFilter filter = (dir, name) -> name.matches(".*epayint.*");

        File[] files = directory.listFiles(filter);

        if (files != null) {
            for (File file : files) {
                if (file.isFile()) {

                    long lastModifiedTimestamp = file.lastModified();
                    Time lastModifiedDate = new Time(lastModifiedTimestamp);
                    logger.info("last date -> {}", lastModifiedDate);

                    if (lastModifiedDate.after(nowTime)){
                        todaysFiles.add(file.getName());
                        logger.info("fileGet -> {}", file.getName());
                    }
                }
            }
        }

        return todaysFiles;
    }

}
