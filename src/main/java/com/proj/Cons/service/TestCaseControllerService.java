package com.proj.Cons.service;

import com.proj.Cons.dto.TestCase;
import com.proj.Cons.epayCons.EpayCons;
import com.proj.Cons.epayCons.EpayStream;
import com.proj.Cons.nwintCons.NwintCons;
import com.proj.Cons.nwintCons.NwintStream;
import com.proj.Cons.rep.TestCaseRep;
import com.proj.Cons.serverProcesses.ServerProcesses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class TestCaseControllerService {

    private static Logger logger = LoggerFactory.getLogger(TestCaseControllerService.class);

    @Autowired
    TestCaseRep testCaseRep;

    @Autowired
    ServerProcesses serverProcesses;

    @Autowired
    EpayCons epayCons;

    @Autowired
    NwintCons nwintCons;

    @Autowired
    NwintStream nwintStream;

    @Autowired
    EpayStream epayStream;

    @Value("${sleep.time}")
    private int sleepTime;

    @Value("${sv.fields}")
    private String svPathToFields;

    @Value("${mastercard.fields}")
    private String mastercardPathToFields;





    public TestCase getTestCaseByID (Long id){
        return testCaseRep.findById(id).orElseThrow();
    }

    public ArrayList<TestCase> getTestCaseDetails(Long id){

        Optional<TestCase> testCase = testCaseRep.findById(id);
        ArrayList<TestCase> testCaseDetails = new ArrayList<>();
        testCase.ifPresent(testCaseDetails::add);

        return testCaseDetails;
    }

    public TestCase testCaseEdit(TestCase testCase, String name, String jsonBody) throws Exception {
        String hexBody = epayCons.buildISOMessage(jsonBody, svPathToFields);
        logger.info("Epay package successfully changed \n{}", hexBody);
        testCase.setTransactionName(name);
        testCase.setJsonBody(jsonBody);
        testCase.setHexBody(hexBody);
        testCase.setNwintJsonBody(null);
        testCase.setNwintHexBody(null);
        testCase.setStan(getStan(jsonBody));
        testCase.setPaymentSystem(getPaymentSystem(jsonBody));

        return testCase;
    }

    public TestCase addTransaction(Long autotestID, String name, String jsonBody) throws Exception {

        String hexBody = epayCons.buildISOMessage(jsonBody, svPathToFields);

        logger.info("Epay transaction {} has been received", name);

        String stan = getStan(jsonBody);
        String paymentSystem = getPaymentSystem(jsonBody);

        TestCase testCase = new TestCase(autotestID, name, jsonBody, hexBody, stan, paymentSystem);


        return testCase;
    }

    public TestCase importTransaction(Long autotestID, String name, String hexBody) throws Exception {

        String jsonBody = epayCons.unpackedISOMessage(hexBody, svPathToFields);

        logger.info("Hex form \"{}\" has been received", name);

        String stan = getStan(jsonBody);
        String paymentSystem = getPaymentSystem(jsonBody);

        TestCase testCase = new TestCase(autotestID, name, jsonBody, hexBody, stan, paymentSystem);


        return testCase;
    }

    public String getStan (String json) throws Exception {

        String jsonBody = json;

        String regex = "\"field\":\\s+\"11\",[^}]+";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(jsonBody);

        String field11 = "";
        String stan = "";

        if (matcher.find()) {
            field11 = matcher.group();
            String arr[] = field11.split(":");
            stan = (arr[2].replaceAll("\"", "")).trim();
            logger.info("stan from field11 {}", stan);

        } else {
            throw new Exception("get Field 11 error");
        }

        return stan;
    }

    public String getPaymentSystem (String json) throws Exception {

        String jsonBody = json;

        String regex = "\"field\":\\s+\"2\",[^}]+";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(jsonBody);

        String field11 = "";
        String paymentSystem = "";

        if (matcher.find()) {
            field11 = matcher.group();
            String arr[] = field11.split(":");
            paymentSystem = (arr[2].replaceAll("\"", "")).trim();
            paymentSystem = String.valueOf(paymentSystem.charAt(0));
            logger.info("payment system from field2 {}", paymentSystem);
            if (paymentSystem.equals("2")){
                paymentSystem = "MIR";
            } else if (paymentSystem.equals("4")){
                paymentSystem = "Visa";
            } else if (paymentSystem.equals("5") || paymentSystem.equals("6")){
                paymentSystem = "MC";
            } else {
                throw new Exception("get Field 2 error, unknown PS");
            }

        } else {
            throw new Exception("get Field 11 error");
        }

        return paymentSystem;
    }



    public TestCase generateNewStan (TestCase testCase) throws Exception {

        String jsonBody = testCase.getJsonBody();

        String regex = "\"field\":\\s+\"11\",[^}]+";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(jsonBody);

        String old11Field = "";

        if (matcher.find()) {
            old11Field = matcher.group();
            logger.info("old11Field -> {}", old11Field);
            Integer value = Math.toIntExact(Math.round(100000 + Math.random() * 900000));

            String new11Field = "\"field\": \"11\",\n" +
                    "      \"value\" : \"" + value + "\"\n    ";

            logger.info("newField -> {}", new11Field);

            jsonBody = jsonBody.replace(old11Field, new11Field);
            String hexBody = epayCons.buildISOMessage(jsonBody, svPathToFields);
            testCase.setJsonBody(jsonBody);
            testCase.setHexBody(hexBody);
            testCase.setNwintHexBody(null);
            testCase.setNwintJsonBody(null);
            testCase.setStan(value.toString());
            testCase.setPaymentSystem(getPaymentSystem(jsonBody));

            testCaseRep.save(testCase);

        } else {
            throw new Exception("Field 11 error");
        }

        return testCase;
    }

    public void sendToSV(TestCase testCase) throws InterruptedException {

        serverProcesses.putIntoData(testCase.getHexBody());
        serverProcesses.startCrTxn();

        Thread.sleep(sleepTime);

    }

    public String getNwintHex(String stan) throws ExecutionException, InterruptedException {

        String utrnno = epayStream.search(stan);
        if (utrnno == null){
            return null;
        }

        String nwintHex = nwintStream.search(utrnno);
        return nwintHex;
    }

    public TestCase getNwintFields(TestCase testCase) throws Exception {

        try {
            String stan = testCase.getStan();
            String paymentSystem = testCase.getPaymentSystem();
            String nwintMes = getNwintHex(stan);
            String jsonBody = "null";

            if (paymentSystem.equals("MIR")){
                jsonBody = epayCons.unpackedISOMessage(nwintMes, mastercardPathToFields);
            } else if (paymentSystem.equals("MC")){
                jsonBody = nwintCons.unpackedMastercardMessage(nwintMes);
            } else if (paymentSystem.equals("Visa")){
                logger.warn("visa nwint not supported");
                jsonBody = "visa nwintHex not supported";
            }


            testCase.setNwintJsonBody(jsonBody);
            testCase.setNwintHexBody(nwintMes);

            testCaseRep.save(testCase);
            return testCase;
        } catch (Exception e){
            logger.warn("doesn't exist utrnno",e);
        }

        testCase.setNwintHexBody("doesn't exist");
        testCase.setNwintJsonBody("doesn't exist");
        testCaseRep.save(testCase);
        return testCase;
    }


}
