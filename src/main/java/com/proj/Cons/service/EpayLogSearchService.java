package com.proj.Cons.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.concurrent.CompletableFuture;

@Service
public class EpayLogSearchService {



    private static Logger logger = LoggerFactory.getLogger(EpayLogSearchService.class);
    private final String message0200 = " 4,0200";


    @Async
    public CompletableFuture<String> Pars (String path, String stan) {
        String filePath = path;
        boolean foundSTAN = false;
        boolean foundFirstString = false;
        boolean flag0200 = false;
        String utrnno = null;


        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            String line;


            while ((line = reader.readLine()) != null) {
                if (line.contains(message0200)) {
                    //logger.info("0200 -> {}", line);
                    flag0200 = true;
                }

                else if (flag0200) {
                    if (line.contains("*************************************************************************************")){
//                        logger.info("line_txrout -> {}", line);
                        flag0200 = false;
                    }

                    else if (line.contains(stan)) {
//                        logger.info("line stan -> {}", line);
                        foundSTAN = true;
                    }

                    else if (foundSTAN){
                        if (!foundFirstString && line.contains("Header Fields")) {
//                            logger.info("line stan -> {}", line);
                            foundFirstString = true;
                        }
                        else if (foundFirstString){
                            //logger.info("line2 -> {}", line);
                            String[] ar = line.split(":");
                            utrnno = (ar[2].trim());
//                            logger.info("utrnno -> {}", utrnno);
                            return CompletableFuture.completedFuture(utrnno);
                        }
                    }


                }
            }

            if (foundSTAN) {
                logger.info("utrnno was found in ePay -> {}", utrnno);
            } else {
                logger.info("utrnno doesn't exist");
            }
        } catch (Exception e) {
            logger.warn("utrnno doesn't exist", e);
        }

        return CompletableFuture.completedFuture(null);
    }

}
