package com.proj.Cons.service;

import com.proj.Cons.model.MailFields;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.Map;

@Service
public class OracleService {


    @Autowired
    @Qualifier("oracleDBJdbcTemplate")
    JdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("oracleDBDataSource")
    DataSource oracleDBDataSource;

    private static Logger logger = LoggerFactory.getLogger(OracleService.class);


    public String executeProcedure(MailFields mailFields) throws Exception {



        logger.info("executeProcedure");

        try {
            jdbcTemplate.update("call email(?,?,?,?,?)",
                    mailFields.getIssuerBin(),
                    mailFields.getRespcode(),
                    mailFields.getSenderDetails(),
                    mailFields.getIssuerDetails(),
                    mailFields.getFooter());
        } catch (Exception e) {
            logger.warn("db problem", e);
            throw new Exception("Error with bd");
        }

        return "success";
    }

    public String executeCheckProcedure(MailFields mailFields) {
        logger.info("executeCheckProcedure");
        logger.info("fields: -> {}, {}, {}, {}, {}",
                mailFields.getIssuerBin(),
                mailFields.getRespcode(),
                mailFields.getSenderDetails(),
                mailFields.getIssuerDetails(),
                mailFields.getFooter());

        Map<String, Object> result = null;
        try {
            result = jdbcTemplate.queryForMap("select check_email(?,?,?,?,?) from dual",
                    mailFields.getIssuerBin(),
                    mailFields.getRespcode(),
                    mailFields.getSenderDetails(),
                    mailFields.getIssuerDetails(),
                    mailFields.getFooter());
        } catch (Exception e) {
            logger.warn("db problem", e);
        }

        return String.valueOf(result.values().stream().iterator().next());
    }
}