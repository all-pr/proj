package com.proj.Cons.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.concurrent.CompletableFuture;

@Service
public class NwintLogSearchService {

    private static Logger logger = LoggerFactory.getLogger(NwintLogSearchService.class);
    private final String message0100 = " 4,0100";
    private final String message0200 = " 4,0200";

    @Async
    public CompletableFuture<String> Pars (String path, String utrnno) {
        String filePath = path;
        boolean foundUTRNNO = false;
        boolean foundFirstString = false;
        boolean flag0100 = false;
        StringBuilder message = new StringBuilder();


        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            String line;


            while ((line = reader.readLine()) != null) {
                if (line.contains(message0100 ) || line.contains(message0200)) {
                    //logger.info("0100 -> {}", line);
                    flag0100 = true;
                }

                else if (flag0100) {
                    if (line.contains("*************************************************************************************")){
                        //logger.info("the end of message -> {}", line);
                        flag0100 = false;
                        foundUTRNNO = false;
                    }

                    else if (line.contains(utrnno) && !foundUTRNNO) {
                        //logger.info("line stan -> {}", line);
                        foundUTRNNO = true;
                    }

                    else if (foundUTRNNO){
                        if (!foundFirstString && line.contains("THE MESSAGE IS")) {
                            //logger.info("line stan -> {}", line);
                            foundFirstString = true;
                        }
                        else if (foundFirstString){
                            if (line.contains("=>get_store_param")
                                    || (line.contains("=>fill_mti_resp (ntwk_utils.c)"))) {
                                return CompletableFuture.completedFuture(message.toString());
                            }
                            String[] ar = line.split("  ",3);
                            message.append(ar[1].trim()).append("\n");
                        }
                    }


                }
            }

            if (foundUTRNNO) {
                logger.info("utrnno was found in nwint -> {}", utrnno);
            } else {
                logger.info("hex doesn't exist in nwint");
            }
        } catch (Exception e) {
            logger.warn("hex doesn't exist in nwint", e);
        }

        return CompletableFuture.completedFuture(null);
    }

}
