package com.proj.Cons.advices;

import com.proj.Cons.annotations.CustomExceptionForEmailSender;
import com.proj.Cons.model.MailFields;
import com.proj.Cons.model.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice(annotations = CustomExceptionForEmailSender.class)
@Controller
public class ExceptionHandlerForEmailSender extends ResponseEntityExceptionHandler {

    private static Logger logger = LoggerFactory.getLogger(ExceptionHandlerForEmailSender.class);


    @ExceptionHandler(Exception.class)
    @GetMapping("/email/error")
    public String handleException(Exception e, Model model, HttpServletRequest req) {


        String url = (String) req.getSession().getAttribute("url");
        req.getSession().removeAttribute("url");

        if (url == "email-builder"){
            MailFields mailFields = (MailFields) req.getSession().getAttribute("sessionMailFields");
            req.getSession().removeAttribute("sessionMailFields");
            model.addAttribute("mailFields", mailFields);

            logger.info("Exception email-builder is working now!");
        }

        Response response = new Response("Error", e.getMessage());
        model.addAttribute("errorMessage", response);

        return url;
    }

}
