package com.proj.Cons.advices;

import com.proj.Cons.annotations.CustomExceptionForTestCases;
import com.proj.Cons.dto.TestCase;
import com.proj.Cons.model.Response;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@ControllerAdvice(annotations = CustomExceptionForTestCases.class)
@Controller
public class ExceptionHandlerForTestcases extends ResponseEntityExceptionHandler {

    @ExceptionHandler(Exception.class)
    @GetMapping("/error")
    public String handleException(Exception e, Model model, HttpServletRequest req) {


        String url = (String) req.getSession().getAttribute("url");
        Long autotestID = (Long) req.getSession().getAttribute("sessionAutotestID");

        req.getSession().removeAttribute("url");
        req.getSession().removeAttribute("sessionAutotestID");

        if (url == "acq-testcase-edit"){

            TestCase testCase = (TestCase) req.getSession().getAttribute("sessionEpayDetails");
            req.getSession().removeAttribute("sessionEpayDetails");
            model.addAttribute("epay", testCase);

            logger.info("Exception edit is working now!");
        }


        if (url == "acq-testcase-add"){
            String name = (String) req.getSession().getAttribute("sessionTransactionName");
            String jsonBody = (String) req.getSession().getAttribute("sessionJsonBody");
            req.getSession().removeAttribute("sessionTransactionName");
            req.getSession().removeAttribute("sessionJsonBody");
            model.addAttribute("transactionName", name);
            model.addAttribute("jsonBody", jsonBody);

            logger.info("Exception add is working now!");
        }

        if (url == "acq-testcase-import"){
            String name = (String) req.getSession().getAttribute("sessionTransactionName");
            String hexBody = (String) req.getSession().getAttribute("sessionHexBody");
            req.getSession().removeAttribute("sessionTransactionName");
            req.getSession().removeAttribute("sessionHexBody");
            model.addAttribute("transactionName", name);
            model.addAttribute("hexBody", hexBody);

            logger.info("Exception import is working now!");
        }

        if (url == "acq-testcase-import-nwint"){
            String hexBody = (String) req.getSession().getAttribute("sessionHexBody");
            Long id = (Long) req.getSession().getAttribute("sessionID");
            req.getSession().removeAttribute("sessionID");
            req.getSession().removeAttribute("sessionHexBody");
            model.addAttribute("hexBody", hexBody);
            model.addAttribute("id", id);

            logger.info("Exception importNwint is working now!");
        }

        if (url == "acq-testcase-updateStan"){

            TestCase testCase = (TestCase) req.getSession().getAttribute("sessionEpayDetails");
            req.getSession().removeAttribute("sessionEpayDetails");
            model.addAttribute("epay", testCase);

            logger.info("Exception generateStan is working now!");

            Response response = new Response("Error", e.getMessage());
            model.addAttribute("errorMessage", response);
            model.addAttribute("autotestID", autotestID);

            return "acq-testcase-details";
        }

        if (url == "acq-testcase-list"){

            List<TestCase> testCase = (List<TestCase>) req.getSession().getAttribute("sessionTestCase");
            req.getSession().removeAttribute("sessionTestCase");
            model.addAttribute("testCase", testCase);

            logger.info("Exception runAll/updateStan/getNwint is working now!");
        }

        if (url == "acq-testcase-details"){

            TestCase testCase = (TestCase) req.getSession().getAttribute("sessionEpayDetails");
            req.getSession().removeAttribute("sessionEpayDetails");
            model.addAttribute("epay", testCase);

            logger.info("Exception resend is working now!");
        }

        Response response = new Response("Error", e.getMessage());
        model.addAttribute("errorMessage", response);
        model.addAttribute("autotestID", autotestID);

        return url;
    }

}
