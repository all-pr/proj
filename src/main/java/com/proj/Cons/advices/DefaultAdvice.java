package com.proj.Cons.advices;

import com.proj.Cons.annotations.CustomExceptionHandlerForEpay;
import com.proj.Cons.model.Response;
import com.proj.Cons.dto.WebView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice(annotations = CustomExceptionHandlerForEpay.class)
@Controller
public class DefaultAdvice extends ResponseEntityExceptionHandler {

    private static Logger logger = LoggerFactory.getLogger(DefaultAdvice.class);

    @ExceptionHandler(Exception.class)
    @GetMapping("/epay/error")
    public String handleException(Exception e, Model model, HttpServletRequest req) {


        String url = (String) req.getSession().getAttribute("url");
        req.getSession().removeAttribute("url");

        if (url == "epay-add"){
            String name = (String) req.getSession().getAttribute("sessionName");
            String jsonBody = (String) req.getSession().getAttribute("sessionJsonBody");
            req.getSession().removeAttribute("sessionName");
            req.getSession().removeAttribute("sessionJsonBody");
            model.addAttribute("name", name);
            model.addAttribute("jsonBody", jsonBody);
        }

        if (url == "epay-import"){
            String name = (String) req.getSession().getAttribute("sessionName");
            String hexBody = (String) req.getSession().getAttribute("sessionHexBody");
            req.getSession().removeAttribute("sessionName");
            req.getSession().removeAttribute("sessionHexBody");
            model.addAttribute("name", name);
            model.addAttribute("hexBody", hexBody);
        }

        if (url == "epay-edit"){
            WebView epayDetails = (WebView) req.getSession().getAttribute("sessionEpayDetails");
            req.getSession().removeAttribute("sessionEpayDetails");
            model.addAttribute("epay", epayDetails);
        }

        if (url == "getter"){
            return e.getMessage();
        }

        if (url == "epay-generateStan"){
            WebView epayDetails = (WebView) req.getSession().getAttribute("sessionEpayDetails");
            req.getSession().removeAttribute("sessionEpayDetails");
            model.addAttribute("epay", epayDetails);
            Response response = new Response("Something wrong",e.getMessage());
            model.addAttribute("errorMessage", response);
            return "epay-details";
        }

        if (url == "epay-import-nwint"){
            Long id = (Long) req.getSession().getAttribute("sessionID");
            String hexBody = (String) req.getSession().getAttribute("sessionHexBody");
            req.getSession().removeAttribute("sessionID");
            req.getSession().removeAttribute("sessionHexBody");
            model.addAttribute("id", id);
            model.addAttribute("hexBody", hexBody);
        }

        Response response = new Response("Incorrect json",e.getMessage());
        model.addAttribute("errorMessage", response);

        return url;
    }

}
