package com.proj.Cons.serverProcesses;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.*;

@Component
public class ServerProcesses {

    private static Logger logger = LoggerFactory.getLogger(ServerProcesses.class);

    @Value("${orgdev.name}")
    private String orgdevName;

    public void putIntoData(String result) {
        String data = "INTERFACE=epayint;\n" +
                "orgdev="+ orgdevName +";\n" +
                "#msgtype=51;\n" +
                "PAUSE=2;\n" +
                "************************************************************************\n";
        String finalData = "\n************************************************************************";
        try (BufferedWriter writer = new BufferedWriter(new FileWriter("/home/data"))) {
            writer.write(data + result + finalData);
            logger.info("the package is packed into date folder -> \n{}\n", data + result + finalData);
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    public void startCrTxn() {
        String[] cmd = new String[]{"sh", "-c", "cd /home; ./cr_txn.sh"};
        try {
            Process proc = Runtime.getRuntime().exec(cmd);

            try (InputStream is = proc.getInputStream()) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                reader.lines().toList();
                logger.info("epay package sent to sv");
            } catch (Exception e){
                logger.error("cr_txn wrong", e);
            }
        } catch (Exception e) {
            logger.error("cr_txn.sh didn't launch", e);
        }
    }

}
