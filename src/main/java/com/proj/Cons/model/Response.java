package com.proj.Cons.model;

import com.fasterxml.jackson.annotation.JsonInclude;

public class Response {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String debugMessage;

    private String message;

    public Response() {
    }

    public Response(String message) {
        this.message = message;
    }

    public Response(String message, String debugMessage) {
        this.message = message;
        this.debugMessage = debugMessage;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDebugMessage() {
        return debugMessage;
    }

    public void setDebugMessage(String debugMessage) {
        this.debugMessage = debugMessage;
    }

    @Override
    public String toString() {
        return  "DebugMessage: " + debugMessage + "\n" +
                "Message: " + message ;
    }
}
