package com.proj.Cons.model;

public class TransactionPackage {

    private String field;
    private String value;

    public TransactionPackage(String field, String value) {
        this.field = field;
        this.value = value;
    }

    @Override
    public String toString() {
        return "Trans{" +
                "field='" + field + '\'' +
                ", value='" + value + '\'' +
                '}';
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}


