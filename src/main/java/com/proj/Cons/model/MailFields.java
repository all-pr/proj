package com.proj.Cons.model;

public class MailFields {


    private String issuerBin;
    private String respcode;
    private String senderDetails;
    private String issuerDetails;
    private String footer;

    public MailFields() {
    }

    public MailFields(String issuerBin, String respcode, String senderDetails, String issuerDetails, String footer) {
        this.issuerBin = issuerBin;
        this.respcode = respcode;
        this.senderDetails = senderDetails;
        this.issuerDetails = issuerDetails;
        this.footer = footer;
    }

    public String getIssuerBin() {
        return issuerBin;
    }

    public void setIssuerBin(String issuerBin) {
        this.issuerBin = issuerBin;
    }

    public String getRespcode() {
        return respcode;
    }

    public void setRespcode(String respcode) {
        this.respcode = respcode;
    }

    public String getSenderDetails() {
        return senderDetails;
    }

    public void setSenderDetails(String senderDetails) {
        this.senderDetails = senderDetails;
    }

    public String getIssuerDetails() {
        return issuerDetails;
    }

    public void setIssuerDetails(String issuerDetails) {
        this.issuerDetails = issuerDetails;
    }

    public String getFooter() {
        return footer;
    }

    public void setFooter(String footer) {
        this.footer = footer;
    }
}
