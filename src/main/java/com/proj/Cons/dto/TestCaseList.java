package com.proj.Cons.dto;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "EC_test_case_list")
public class TestCaseList {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column (name = "ID")
    private Long id;

    @Column (name = "TEST_CASE_NAME")
    private String testCaseName;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "TEST_CASE_ID", referencedColumnName = "ID")
    private List<TestCase> testCases;


    public TestCaseList() {
    }

    public TestCaseList(String testCaseName) {
        this.testCaseName = testCaseName;
    }

    public TestCaseList(String testCaseName, List<TestCase> testCases) {
        this.testCaseName = testCaseName;
        this.testCases = testCases;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTestCaseName() {
        return testCaseName;
    }

    public void setTestCaseName(String testCaseName) {
        this.testCaseName = testCaseName;
    }

    public List<TestCase> getTestCases() {
        return testCases;
    }

    public void setTestCases(List<TestCase> testCases) {
        this.testCases = testCases;
    }

    @Override
    public String toString() {
        return "TestCaseList{" +
                "id =" + id +
                ", testCaseName ='" + testCaseName + '\'' +
                ", testCases =" + testCases +
                '}';
    }
}
