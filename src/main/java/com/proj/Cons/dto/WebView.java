package com.proj.Cons.dto;

import javax.persistence.*;

@Entity
@Table(name = "EC_webview")
public class WebView {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    @Column (name = "JSON_BODY", length = 4000)
    private String jsonBody;
    @Column (name = "HEX_BODY", length = 4000)
    private String hexBody;
    @Column(name = "NWINT_JSON_BODY", length = 4000)
    private String nwintJsonBody;
    @Column(name = "NWINT_HEX_BODY", length = 4000)
    private String nwintHexBody;
    @Column(name = "STAN", length = 10)
    private String stan;


    public WebView() {
    }

    public WebView(String name, String jsonBody, String hexBody, String stan) {
        this.name = name;
        this.jsonBody = jsonBody;
        this.hexBody = hexBody;
        this.stan = stan;
    }

    public WebView(String name, String jsonBody, String hexBody, String nwintJsonBody, String nwintHexBody, String stan) {
        this.name = name;
        this.jsonBody = jsonBody;
        this.hexBody = hexBody;
        this.nwintJsonBody = nwintJsonBody;
        this.nwintHexBody = nwintHexBody;
        this.stan = stan;
    }

    public String getStan() {
        return stan;
    }

    public void setStan(String stan) {
        this.stan = stan;
    }

    public String getNwintJsonBody() {
        return nwintJsonBody;
    }

    public void setNwintJsonBody(String nwintJsonBody) {
        this.nwintJsonBody = nwintJsonBody;
    }

    public String getNwintHexBody() {
        return nwintHexBody;
    }

    public void setNwintHexBody(String nwintHexBody) {
        this.nwintHexBody = nwintHexBody;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getJsonBody() {
        return jsonBody;
    }

    public void setJsonBody(String jsonBody) {
        this.jsonBody = jsonBody;
    }

    public String getHexBody() {
        return hexBody;
    }

    public void setHexBody(String hexBody) {
        this.hexBody = hexBody;
    }
}
