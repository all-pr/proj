package com.proj.Cons.dto;

import javax.persistence.*;

@Entity
@Table(name = "EC_test_case")
public class TestCase {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column (name = "ID")
    private Long id;
    @Column(name = "TEST_CASE_ID")
    private Long listId;
    @Column(name = "TRANSACTION_NAME")
    private String transactionName;
    @Column(name = "JSON_BODY", length = 4000)
    private String jsonBody;
    @Column(name = "HEX_BODY", length = 4000)
    private String hexBody;
    @Column(name = "NWINT_JSON_BODY", length = 4000)
    private String nwintJsonBody;
    @Column(name = "NWINT_HEX_BODY", length = 4000)
    private String nwintHexBody;
    @Column(name = "STAN", length = 10)
    private String stan;
    @Column(name = "PS", length = 10)
    private String paymentSystem;

    public TestCase() {
    }

    public TestCase(String transactionName, String jsonBody, String hexBody, String stan, String paymentSystem) {
        this.transactionName = transactionName;
        this.jsonBody = jsonBody;
        this.hexBody = hexBody;
        this.stan = stan;
        this.paymentSystem = paymentSystem;
    }

    public TestCase(Long listId, String transactionName, String jsonBody, String hexBody, String stan, String paymentSystem) {
        this.listId = listId;
        this.transactionName = transactionName;
        this.jsonBody = jsonBody;
        this.hexBody = hexBody;
        this.stan = stan;
        this.paymentSystem = paymentSystem;
    }

    public TestCase(Long listId, String transactionName, String jsonBody, String hexBody, String nwintJsonBody, String nwintHexBody, String stan, String paymentSystem) {
        this.listId = listId;
        this.transactionName = transactionName;
        this.jsonBody = jsonBody;
        this.hexBody = hexBody;
        this.nwintJsonBody = nwintJsonBody;
        this.nwintHexBody = nwintHexBody;
        this.stan = stan;
        this.paymentSystem = paymentSystem;
    }

    public String getPaymentSystem() {
        return paymentSystem;
    }

    public void setPaymentSystem(String paymentSystem) {
        this.paymentSystem = paymentSystem;
    }

    public String getStan() {
        return stan;
    }

    public void setStan(String stan) {
        this.stan = stan;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getListId() {
        return listId;
    }

    public void setListId(Long listId) {
        this.listId = listId;
    }

    public String getTransactionName() {
        return transactionName;
    }

    public void setTransactionName(String transactionName) {
        this.transactionName = transactionName;
    }

    public String getJsonBody() {
        return jsonBody;
    }

    public void setJsonBody(String jsonBody) {
        this.jsonBody = jsonBody;
    }

    public String getHexBody() {
        return hexBody;
    }

    public void setHexBody(String hexBody) {
        this.hexBody = hexBody;
    }

    public String getNwintJsonBody() {
        return nwintJsonBody;
    }

    public void setNwintJsonBody(String nwintJsonBody) {
        this.nwintJsonBody = nwintJsonBody;
    }

    public String getNwintHexBody() {
        return nwintHexBody;
    }

    public void setNwintHexBody(String nwintHexBody) {
        this.nwintHexBody = nwintHexBody;
    }
}
