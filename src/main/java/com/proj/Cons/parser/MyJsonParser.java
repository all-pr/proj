package com.proj.Cons.parser;

import com.proj.Cons.model.TransactionPackage;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MyJsonParser {

    private static Logger logger = LoggerFactory.getLogger(MyJsonParser.class);

//    private static final String PATH_TO_JSON = "transPackage.json";

    public List<TransactionPackage> parse(String jsonBody)  {

        JSONParser jsonParser = new JSONParser();

        List<TransactionPackage> trans = new ArrayList<>();

        try {

            JSONObject jsonObject = (JSONObject) jsonParser.parse(jsonBody);

            JSONArray JSONFields = (JSONArray) jsonObject.get("fields");

            for (Object it : JSONFields){
                JSONObject fields = (JSONObject) it;

                if (compositeFieldExists(trans, fields)) continue;

                String field = (String) fields.get("field");
                String value = (String) fields.get("value");
                trans.add(new TransactionPackage(field, value));

            }

        } catch (Exception e) {
            logger.error("Json parser wrong", e);
        }

        return trans;
    }

    private boolean compositeFieldExists(List<TransactionPackage> trans, JSONObject fields) {
        if(fields.get("field").equals("47") || fields.get("field").equals("48") || fields.get("field").equals("115")){

            String jsonField = fields.get("value").toString();
            if (jsonField.contains("tag")){
                JSONArray JsonCompositeField = (JSONArray) fields.get("value");

                StringBuilder sb = new StringBuilder();
                for (Object it : JsonCompositeField){
                    JSONObject compositeField = (JSONObject) it;

                    sb.append(compositeField.get("tag"));
                    sb.append(compositeField.get("length"));
                    sb.append(compositeField.get("value"));
                }
                String field = (String) fields.get("field");
                String value = sb.toString();
                trans.add(new TransactionPackage(field, value));

            } else {
                String field = (String) fields.get("field");
                String value = (String) fields.get("value");
                trans.add(new TransactionPackage(field, value));
            }
            return true;
        }
        return false;
    }
}

