package com.proj.Cons.parser;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class JsonMessagePrint {

    private static Logger logger = LoggerFactory.getLogger(JsonMessagePrint.class);

    public String printJsonMessage(ISOMsg isoMsg) throws ISOException {
        String data = "{\n" + "  \"fields\": [";
        String finalData = "  ]\n" + "}";
        String body = "";
        StringBuilder sb = new StringBuilder();
        body = "{\n" +
                "      \"field\": \"0\",\n" +
                "      \"value\" : \"" + isoMsg.getMTI() + "\"\n" +
                "    },";
        sb.append(body);

        for (int i = 1; i <= isoMsg.getMaxField(); i++) {
            if (isoMsg.hasField(i)) {
                if (i == isoMsg.getMaxField()){
                    body = "{\n" +
                            "      \"field\": \"" + i + "\",\n" +
                            "      \"value\" : \"" + isoMsg.getString(i) + "\"\n" +
                            "    }";
                    sb.append(body);
                    continue;
                }
                body = "{\n" +
                        "      \"field\": \"" + i + "\",\n" +
                        "      \"value\" : \"" + isoMsg.getString(i) + "\"\n" +
                        "    },";

                sb.append(body);
            }
        }
        body = sb.toString();


        String result = data + body + finalData;
        //logger.info("result = {}", result);

        return result;
    }

}
