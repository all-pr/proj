package com.proj.Cons.config;

import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

@Configuration
public class DataSourceConfiguration {

    @Bean
    @Primary
    public DataSource derbyDBDataSource() {
        return derbyDBDataSourceProperties()
                .initializeDataSourceBuilder()
                .build();
    }

    @Bean
    @ConfigurationProperties("spring.datasource.derby-db")
    public DataSourceProperties derbyDBDataSourceProperties() {
        return new DataSourceProperties();
    }

    @Bean
    public JdbcTemplate derbyDBJdbcTemplate() {
        return new JdbcTemplate(derbyDBDataSource());
    }


    @Bean
    public DataSource oracleDBDataSource() {
        return oracleDBDataSourceProperties()
                .initializeDataSourceBuilder()
                .build();
    }

    @Bean
    @ConfigurationProperties("spring.datasource.oracle-db")
    public DataSourceProperties oracleDBDataSourceProperties() {
        return new DataSourceProperties();
    }

    @Bean
    public JdbcTemplate oracleDBJdbcTemplate() {
        return new JdbcTemplate(oracleDBDataSource());
    }
}
