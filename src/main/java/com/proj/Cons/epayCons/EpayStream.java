package com.proj.Cons.epayCons;

import com.proj.Cons.service.EpayLogSearchService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Component
public class EpayStream {

    private static Logger logger = LoggerFactory.getLogger(EpayStream.class);

    @Autowired
    EpayLogSearchService fileSearchService;

    @Value("${path.to.epay}")
    private String path;

    @Value("${contour}")
    private String contour;

    @Value(("${epay1}"))
    private String epay1;
    @Value(("${epay2}"))
    private String epay2;
    @Value(("${epay3}"))
    private String epay3;
    @Value(("${epay4}"))
    private String epay4;

    CompletableFuture<Object> combinedResult;
    List<String> epayFiles;
    List<CompletableFuture<?>> streams;

    public String search (String stan) throws ExecutionException, InterruptedException {

        if (contour.equals("test")){
            epayFiles = List.of(epay1);
        } else if (contour.equals("stage")){
            epayFiles = List.of(epay1, epay2, epay3, epay4);
        } else {
            logger.error("value \"contur\" is incorrect");
        }

        streams = new ArrayList<>();

        for (String epayFile : epayFiles) {
            CompletableFuture<String> file1Result = fileSearchService.Pars(path + epayFile, stan);
            streams.add(file1Result);

//            combinedResult = CompletableFuture.anyOf(file1Result);
//
//            if (combinedResult.get() != null) {
//                logger.info("epay name -> {}", epayFile);
//                break;
//            }
        }

        CompletableFuture<Object> res = CompletableFuture.anyOf(streams.stream()
                .map(rf -> rf
                        .exceptionally(th -> null)
                        .thenCompose(rr -> rr == null ? null : CompletableFuture.completedFuture(rr)))
                .toArray(CompletableFuture[]::new));

        try {
            String utrnno = (String) res.get();
            if (utrnno != null) {
                logger.info("utrnno was found in ePay -> {}", utrnno);
                return utrnno;
            } else {
                logger.info("there is no UTRNNO in all ePay files");
                return null;
            }
        } catch (Exception e) {
            logger.warn("error during ePay thread operation", e);
            return null;
        }

    }

}
