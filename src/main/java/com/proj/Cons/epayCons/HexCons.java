package com.proj.Cons.epayCons;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class HexCons {

    private static Logger logger = LoggerFactory.getLogger(HexCons.class);


    public String toHex(byte[] a, String bitMap){

        StringBuilder sb = new StringBuilder(a.length * 2);
        int counter = 0;
        for(byte b: a) {
            sb.append(String.format("%02X", b));
        }
        String hexFormat = sb.toString();
        String delOldBitMap = hexFormat.substring(8, hexFormat.indexOf("3136"));
        hexFormat = hexFormat.replace(delOldBitMap, bitMap);

        StringBuilder finalISOsb = new StringBuilder();

        for (int i = 0; i < hexFormat.length(); i = i + 2){
            finalISOsb.append(hexFormat.charAt(i));
            finalISOsb.append(hexFormat.charAt(i + 1));
            finalISOsb.append('.');
            counter++;
            if (counter == 16){
                finalISOsb = new StringBuilder(finalISOsb.substring(0, finalISOsb.length() - 1));
                finalISOsb.append('\n');
                counter = 0;
            }

        }
        String epayHexFormat = finalISOsb.toString();
        epayHexFormat = epayHexFormat.substring(0, epayHexFormat.length() - 1);

        return epayHexFormat;
    }
}
