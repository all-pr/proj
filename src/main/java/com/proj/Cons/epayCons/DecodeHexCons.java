package com.proj.Cons.epayCons;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;

@Component
public class DecodeHexCons {

    private static Logger logger = LoggerFactory.getLogger(DecodeHexCons.class);


    public byte[] decodeHex(String svHex) throws DecoderException {

        String newHex = svHex.replaceAll("\\s", "");
        logger.info("hex -> {}", newHex);
        newHex = newHex.replaceAll("\\.", "");

        String bitMap = newHex.substring(8, newHex.indexOf("3136"));
        logger.info("bitMap {}", bitMap);

        String hexFormat = Hex.encodeHexString(bitMap.getBytes(StandardCharsets.UTF_8));
        logger.info("bit map to hex format {}", hexFormat);

        newHex = newHex.replace(bitMap, hexFormat);
        logger.info("final hex {}", newHex);

        byte[] byteHex = Hex.decodeHex(newHex);
        logger.info("bytehex -> {}", byteHex);

        return byteHex;
    }

}
