package com.proj.Cons.epayCons;

import com.proj.Cons.model.TransactionPackage;
import com.proj.Cons.parser.JsonMessagePrint;
import com.proj.Cons.parser.MyJsonParser;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.packager.GenericPackager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class EpayCons {

    @Autowired
    HexCons hexCons;

    @Autowired
    DecodeHexCons decodeHexCons;

    private static Logger logger = LoggerFactory.getLogger(EpayCons.class);

    private static String bitMap;

    MyJsonParser jsonParser = new MyJsonParser();
    JsonMessagePrint jsonMessagePrint = new JsonMessagePrint();
    List<TransactionPackage> trans;
    private String mtiValue;

    public String buildISOMessage(String jsonBody, String path) throws Exception {
        try {
            trans = jsonParser.parse(jsonBody);
            GenericPackager packager = new GenericPackager(path);

            trans.stream().filter(f -> f.getField().equals("0")).forEach(m -> {
                mtiValue = m.getValue();
            });
            ISOMsg isoMsg = new ISOMsg();
            isoMsg.setPackager(packager);
            isoMsg.setMTI(mtiValue);
            for (TransactionPackage getFields : trans){
                isoMsg.set(getFields.getField(), getFields.getValue());
            }

            byte[] result = isoMsg.pack();

            String fullISO = new String(result);
            bitMap = fullISO.substring(fullISO.indexOf(isoMsg.getMTI()) + 4, fullISO.indexOf("16"));
            //logger.info("bitMap = {}", bitMap);

            //printISOMessage(isoMsg);

            String epayISOpackage = hexCons.toHex(result, bitMap);


            return epayISOpackage;
        } catch (ISOException e) {
            throw new Exception(e);
        }
    }

    public String unpackedISOMessage(String svHex, String path) throws Exception {


        byte[] mes = decodeHexCons.decodeHex(svHex);

        try {
            GenericPackager packager = new GenericPackager(path);
            //System.out.println(Arrays.toString(mes));
            ISOMsg isoMsg = new ISOMsg();
            isoMsg.setPackager(packager);
            isoMsg.unpack(mes);

            String jsonMessage = jsonMessagePrint.printJsonMessage(isoMsg);

            //byte[] result = isoMsg.pack();
            return jsonMessage;
        } catch(ISOException e) {
            throw new Exception(e);
        }

    }

    private void printISOMessage(ISOMsg isoMsg) {
        try {
            logger.info("MTI = {}", isoMsg.getMTI());
            for (int i = 1; i <= isoMsg.getMaxField(); i++) {
                if (isoMsg.hasField(i)) {
                    logger.info("Field {} = {}", i, isoMsg.getString(i));
                }
            }
        } catch (ISOException e) {
            e.printStackTrace();
        }
    }



}

