package com.proj.Cons.rep;

import com.proj.Cons.dto.TestCaseList;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TestCaseListRep extends JpaRepository<TestCaseList, Long> {
}
