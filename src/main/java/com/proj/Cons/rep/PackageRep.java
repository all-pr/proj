package com.proj.Cons.rep;

import com.proj.Cons.dto.WebView;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PackageRep extends JpaRepository<WebView, Long> {
}
