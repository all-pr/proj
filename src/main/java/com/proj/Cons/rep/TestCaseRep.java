package com.proj.Cons.rep;

import com.proj.Cons.dto.TestCase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface TestCaseRep extends JpaRepository<TestCase, Long> {

    @Query(value = "SELECT * FROM EC_test_case WHERE TEST_CASE_ID = :testCaseId" , nativeQuery = true)
    TestCase findByTestCaseId(Long testCaseId);

}
